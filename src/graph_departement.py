import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import pants
import math
import random
import map_generator as mg
import pickle

def get_french_department(zipcode):
    if pd.isnull(zipcode):
        return 0
    return  int(zipcode) // 1000

# Load csv file
path_db = '../data/db/pylones-rte.csv'
df = pd.read_csv(path_db, sep=';', usecols=[8,9], low_memory=False)

# utiliser data/processed/strategic-companies.csv pour en extraire la liste des entreprises
def show_most_strategic_nuclear_department():
    companies = pd.read_csv('data/processed/strategic_companies.csv')
    #includes also ex nuclear department
    valid_department = [1, 8, 29, 30, 37, 38, 41, 68, 82]
    #filter based on department
    companies.iloc[:, 5] = companies.iloc[:, 5].apply(get_french_department)
    companies = companies[companies.iloc[:, 5].isin(valid_department)]
    plt.hist(companies.iloc[:, 5], valid_department)
    plt.show()

def euclidean(a, b):
    return math.sqrt(pow(a[1] - b[1], 2) + pow(a[0] - b[0], 2))

#apply ant optimisation program on a df to find pseudo optimal graph
def ant_simulation(df):
    longitude_min, longitude_max = 4.0979,4.6802 
    latitude_min, latitude_max = 45.2440,45.6275  

    df = df.apply(pd.to_numeric)
    df  = df[(longitude_min <= df['Longitude pylône (DD)'])
        & (pd.to_numeric(df['Longitude pylône (DD)']) <= longitude_max)]
    df  = df[(latitude_min <= df['Latitude pylône (DD)'])
        & (pd.to_numeric(df['Latitude pylône (DD)']) <= latitude_max)]
    df = df.drop_duplicates()

    world = pants.World(df.values.tolist(), euclidean)
    solver = pants.Solver()
    solution = solver.solve(world)
<<<<<<< HEAD
    with open('../data/ligne_THT.graph', 'wb') as graph:
        pickle.dump(solution.tour, graph)
=======
    with open('data/db/finistere_THT.graph', 'wb') as graph:
        pickle.dump(solution.tour, graph)

#pickle.load?
#find a way to use finistere_tht.graph generated
>>>>>>> master
