import geoviews
import csv
from Tools.scripts.objgraph import ignore
from time import sleep
from geopy import GoogleV3
from geopy.geocoders import Nominatim
import pandas as pd
import jupyter
import IPython
import pyproj
import scipy
import hvplot.pandas


from sklearn.cluster import DBSCAN
import numpy as np
import matplotlib.pyplot as plt

df_clust = df_coord.iloc[:, [2,3]]
X = df_clust.to_numpy()
clustering = DBSCAN(eps=0.2, min_samples=5).fit(X)


naf_strategic = [
    ["25.40Z", "Fabrication d'armes et de munitions"],
    ["29.10Z", "Construction de véhicules automobiles"],
    ["30.11Z", "Construction de navires et de structures flottantes"],
    ["30.30Z", "Construction aéronautique et spatiale"],
    ["30.40Z", "Construction de véhicules militaires de combat"],
    ["33.11Z", "Réparation d'ouvrages en métaux"]
]

coordinates = "data/processed/addresses_to_coordinates.csv"
companies = "data/processed/strategic_companies.csv"
companies_bdd = "data/StockEtablissement_utf8.csv"

# here the naf that we've considered as strategic
naf_strategic = [
    ["25.40Z", "Fabrication d'armes et de munitions"],
    ["29.10Z", "Fabrication de véhicules blindés pour le transport de fonds"],
    ["30.11Z", "Implantation  des  armements  sur  bâtiments  de  guerre"],
    ["30.30Z", "Fabrication de missiles balistiques"],
    ["30.40Z", "Fabrication de chars et d'autres véhicules blindés de combat"],
    ["33.11Z", "Entretien et de réparation de véhicules militaires de combat"]
]

geolocator = GoogleV3(api_key="AIzaSyADXUPDTbVwnAfhWxMeSiGms_tTiSuQ4SU", timeout=3)

def get_coordinates(address):
    """ get coordinates from an address using the OpenStreetMaps API"""
    data = geolocator.geocode(address)
    if not data:
        return False
    return [data.point.latitude, data.point.longitude, data.raw]


def add_an_entry(address, siret):
    """ cache addresses and coordinates into a csv file """
    f = open(coordinates, "r+")
    r = csv.reader(f, delimiter=",")
    for row in r:
        if row[1] == address:
            print("# add_an_entry | found")
            return
    w = csv.writer(f, delimiter=",")
    coord = get_coordinates(address)
    if coord:
        print("# add_an_entry | added")
        w.writerow([siret, address] + coord)
        sleep(0.5)
    else:
        print("# add_an_entry | failed")
        w.writerow([siret, address] + [0.0, 0.0])
    f.close()


def get_coordinates_from_cache(address):
    """ extract coordinates from the csv """
    f = open(coordinates, "r+")
    r = csv.reader(f, delimiter=",")
    for row in r:
        if row[1] == address:
            f.close()
            return float(row[2]), float(row[3])
    f.close()
    return False


def is_strategic(naf):
    """ return if the naf in input is strategic """
    for a, b in naf_strategic:
        if a == naf:
            return True
    return False


def compare_department(l, department):
    """ return if the postcode is in the department list"""
    for el in l:
        if el // 100 in department:
            return True
    return False


def extract_strategic_companies():
    """ extract the companies with a strategic naf from the INSEE dataset """
    naf = [el[0] for el in naf_strategic]
    # cleaning the companies file
    open(companies, "w").close()
    # processing the bdd iteratively
    for chunk in pd.read_csv(companies_bdd, sep=',', chunksize=100000, usecols=[2, 12, 14, 15, 16, 17, 25 + 20],
                             low_memory=False):
        subset = chunk[chunk['activitePrincipaleEtablissement'].isin(naf)]
        subset.to_csv(companies, mode='a', header=False, index=False)


def pretty_address(row):
    """ format the address from row """
    addr = ""
    # numeroVoieEtablissement
    if not pd.isna(row[1]):
        addr += str(int(row[1])) + ' '
    # typeVoieEtablissement
    if not pd.isna(row[2]):
        addr += str(row[2]) + ' '
    # libelleVoieEtablissement
    addr += str(row[3]) + ', ' + str(int(row[4])) + ' ' + str(row[5]) + ", FRANCE"
    print("-> ", addr)
    return addr


def extract_companies_coordinates():
    """ extract the coordinates of the strategic companies """
    df = pd.read_csv(companies, sep=',')
    for index, row in df.iterrows():
        # libelleVoieEtablissement,codePostalEtablissement
        if pd.isna(row[3]) or pd.isna(row[4]):
            print("-> ignoring")
            continue
        add_an_entry(pretty_address(row), row[0])


if __name__ == "__main__":
    pd.options.display.max_columns = None
    # extract_strategic_companies()
    # extract_companies_coordinates()
    print("hello")
