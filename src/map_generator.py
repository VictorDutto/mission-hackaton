import pandas as pd
import matplotlib.pyplot as plt
import os


# Parameters are tuples of shape: (min_value, max_value)
def init_BBox(longitudes, latitudes):
    return (longitudes[0], longitudes[1], latitudes[0], latitudes[1])


def create_title_from_path(path_map):
    filename = os.path.basename(path_map)
    base_filename = filename.split('.')[0]
    l = base_filename.split('_')

    title = ''
    for elt in l:
        title += elt.upper()
        title += ' '

    return title


# BBox: cf init_BBox
# path_map: path to PNG file map
# coord: tuple of shape: (df['longitude'], df['latitude'])
def plot_map(BBox, path_map, coord):
    df = pd.DataFrame(list(zip(coord[0], coord[1])),
                      columns=['longitude', 'latitude'])

    ruh_m = plt.imread(path_map)

    fig, ax = plt.subplots(figsize=(8, 7))

    ax.scatter(coord[0], coord[1], zorder=1, alpha=1, c='b', s=20)

    title = create_title_from_path(path_map)
    ax.set_title(title)
    ax.set_xlim(BBox[0], BBox[1])
    ax.set_ylim(BBox[2], BBox[3])

    ax.imshow(ruh_m, zorder=0, extent=BBox, aspect='equal')
    plt.show()


if __name__ == "__main__":
    interval_long = (-4.779, 8.328)
    interval_lat = (42.277, 51.083)
    BBox = init_BBox(interval_long, interval_lat)

    path_db = '../data/res/processed_data_Hydraulique.csv'
    data = pd.read_csv(path_db, sep=';', encoding='UTF-8')

    path_map = "../data/png/map_france.PNG"
    plot_map(BBox, path_map, (data['longitude'], data['latitude']))
