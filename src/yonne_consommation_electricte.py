import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# print('ok')

## Print as many columns as we want
#desired_width = 320
#nb_columns = 25

#pd.set_option('display.width', desired_width)
#np.set_printoptions(linewidth=desired_width)
#pd.set_option('display.max_columns', nb_columns)

## Load csv file
path_db = "../data/db/consommation-annuelle-a-la-maille-adresse-2018-et-2019.csv"
df = pd.read_csv(path_db, sep=";")

# Take data from 2018 (as 2019 are incomplete, need to work on that)
# Keep only cities that have at least two different energy sources (electricity + gas)
yonne = df[df['departement'] == 'Yonne']
yonne = yonne.dropna(subset=['consommation_totale'])

yonne_by_address = yonne.groupby('commune').mean()
yonne_by_address = yonne_by_address.sort_values(by=['consommation_totale'])

#get the mean value associated with each address
# whats behind: each value with the same address IS from a different year
# Use seaborn to barplot
sns.barplot(yonne_by_address.index, yonne_by_address['consommation_totale'])
plt.show()
